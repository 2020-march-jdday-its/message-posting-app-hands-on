﻿﻿$(document).ready(function() {
    getMessages();
    setInterval(getMessages, 1000);
});

function getMessages() {
    $.ajax({
        type: "GET",
        url: "/api/json-msg",
        dataType: "text"
    })
        .done(function(response) {
            $("#divMessages").html(buildMessageList(response));
        })
        .fail(function( $xhr ) {

        });//END Ajax call
}

function buildMessageList(messagesJson) {
    var html = '';
    if (messagesJson) {
        var messages = JSON.parse(messagesJson);
        for (var i = 0; i < messages.length; i++) {
            var m = messages[i];
            html += '<div class="row">';
            html += ' <div class="col-lg-12">';
            html += '  <div class="alert alert-primary" role="alert">';
            html += '   <h4 class="alert-heading">' + m.name + '</h4>';
            html += '   <p>' + m.msg + '</p>';
            html += '  </div>';
            html += ' </div>';
            html += '</div>';
        }
    }
    return html;
}