﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
 using MessagePostingApp.Model;

 namespace MessagePostingApp.Services {
    public class MessagesService {
        
        private readonly CensoringService censoringService;
        private Queue<Message> Messages;
        
        public MessagesService(CensoringService _censoringService) {
            Messages = new Queue<Message>();
            censoringService = _censoringService;
        }

        public void AddMessage(Message message) {
            if (censoringService.ContainsCensoredWords(message.Name)
                || censoringService.ContainsCensoredWords(message.Msg)) {
                //Not enqueue bad messages
                return;
            }
            Messages.Enqueue(message);
        }

        public IEnumerable GetMessages() {
            return Messages;
        }

    }
}